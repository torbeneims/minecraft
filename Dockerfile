FROM openjdk:17-alpine

WORKDIR /usr/src/app

#COPY server.jar server.jar

EXPOSE 25565

CMD ["java", "-jar", "server.jar"]
